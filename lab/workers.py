from celery import Celery
import time

celery = Celery('random.org16')

class Config:
    BROKER_URL='amqp://194.29.175.241'
    CELERY_RESULT_BACKEND = 'cache'
    CELERY_CACHE_BACKEND = 'memcached://194.29.175.241:6756'
    CELERY_DEFAULT_QUEUE = 'p16'

celery.config_from_object(Config)

@celery.task
def compute_random(req):
    randomorg = []
    req = req.split('\n')
    for item in req:
        try:
            number = int(item)
            randomorg.append(number)
        except:
            pass
    randomorg.sort()
    return randomorg

if __name__ == '__main__':
    celery.start()
